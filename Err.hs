module Err (Err (), ErrT (), try, throw, runErrT) where

import Control.Monad.Except

data Err = Err [String] deriving (Eq)
type ErrT = ExceptT Err

instance Show Err where
	show (Err t) = unlines $ reverse t

try :: Monad m => String -> ErrT m a -> ErrT m a
try bl pc = catchError pc (\(Err bledy) -> throwError $ Err (bl:bledy))

throw :: Monad m => String -> ErrT m a
throw = throwError . Err . (:[])

runErrT = runExceptT
