module Main where

import Constructions.Lex
import Constructions.Par
import Constructions.Abs
import Constructions.ErrM
import Constructions.Print
import Desugar
import DoScopes
import Env
import System.IO
import System.IO.Error
import System.Exit
import System.Environment
import Stm
import Type
import Control.Monad
import Control.Monad.IO.Class
import Verify

main :: IO ()
main = do
	hSetBuffering stdout NoBuffering
	args <- getArgs
	src <- case args of
		[src_file] -> readFile src_file
		[] -> hGetContents stdin
	catchIOError (main' src) $ \e -> do
		hPutStrLn stderr (ioeGetErrorString e)
		exitFailure
	exitSuccess

main' :: String -> IO ()
main' s = do
	case pProgram $ myLexer s of
		Ok tree -> do
			let tree' = (doScopes . desugar) tree
			verify tree'
			tree'' <- doTypeCheck tree'
			debug "### AST ###"
			debug $ printTree tree''
			debug "### exec ###"
			res <- runEnv $ execProg tree''
			debug (show res)
		Bad s -> fail $ "parse error: " ++ show s

--debug s = liftIO (hPutStrLn stderr s)
debug _ = return ()
