{-# LANGUAGE TypeFamilies #-}
module Type (doTypeCheck) where

import Constructions.Abs
import Constructions.Print
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Class
import qualified Control.Monad.Reader as R
import qualified Control.Monad.State as S
import qualified Data.Map as M
import qualified ListT as L
import Data.List
import Err
import Data.Either
import qualified Data.Functor.Identity as I
import GHC.Exts (sortWith)
import qualified Data.Set as S


type TypeEnv =
	ErrT (
		--         variable -> type   typedefs
		R.ReaderT (M.Map Ident Type,  M.Map Ident Type) (
			--        tmp type -> type,   free types
			S.StateT (M.Map Integer Type, [Type])     (L.ListT I.Identity)
		)
	)

doTypeCheck :: Program -> IO Program
doTypeCheck (StmProgram stm) = do
	let res = unpack $ do
		(a, t, _) <- retType TUnknown [TTuple []] stm
		t' <- normalizeTypes t
		return (a, t')
	let (nook, ok) = partitionEithers $ fmap (\(result, (state, _)) -> case result of
			Left a -> Left (a, state)
			Right (_, a) -> Right (a, state)
		) res
	(stm', _) <- case ok of
		[(result, state)] -> return (result, state)
		[] -> fail $ "typecheck error: all branches failed:\n" ++ (unlines $ nub $ fmap (show . fst) nook)
		_ -> fail $ "typecheck error: too many typings (" ++ show (length ok) ++ "):\n" ++ (unlines $ nub $ fmap (printTree . fst) ok)
	return $ StmProgram stm'
	where
		unpack all = noId where
			noErr = runErrT all
			noReader = R.runReaderT noErr (M.empty, M.empty)
			noState = S.runStateT noReader (M.empty, freeTypePool)
			noList = L.toList noState
			noId = I.runIdentity noList
			freeTypePool = TTmp <$> [0..]

newType :: TypeEnv Type
newType = do
	(bindings, (t:free)) <- S.get
	S.put (bindings, free)
	return t

newTypeScope :: [Ident] -> [ScopeTypedef] -> TypeEnv a -> TypeEnv (a, [Type])
newTypeScope idents types c = do
	newTypes <- mapM (const newType) idents
	let readerUpdate = M.fromList $ zip idents newTypes
	let newTypedefs = M.fromList $ fmap (\(ScopeTypedef name typ) -> (name, typ)) types
	R.local (\(vars, typedefs) -> (M.union readerUpdate vars, M.union newTypedefs typedefs)) $ do
		c' <- c
		return (c', newTypes)

typeOf :: Exp -> TypeEnv (Type, Exp)
typeOf exp = do
	t <- newType
	exp' <- checkType t exp
	return (t, exp')

checkType :: Type -> Exp -> TypeEnv Exp
checkType t (ETypeBound exp typ) = do
	matchType t typ
	checkType t exp
checkType t exp@(EIdent ident) = do
	tt <- R.asks $ (M.lookup ident . fst)
	case tt of
		Just typ -> do
			matchType typ t
			return exp
		Nothing -> error $ "not in scope: " ++ show ident
checkType t (EAssignment target exp) = do
	target' <- checkType t target
	exp' <- checkType t exp
	return $ EAssignment target' exp'
checkType t (ECall f args) = do
	(tin, args') <- liftM unzip $ mapM typeOf args
	f' <- checkType (TFunction (TTuple tin) t) f
	return $ ECall f' args'
checkType t (ETuple exps) = do
	ts <- mapM (const newType) exps
	matchType t (TTuple ts)
	exps' <- mapM (uncurry checkType) (zip ts exps)
	return $ ETuple exps'
checkType t (EConstruction params stm) = do
	TFunction tintuple tout <- matchType (TFunction TUnknown TUnknown) t
	(stm', tin) <- newTypeScope params [] (do
			(expst, stm', _) <- retType tout [TTuple []] stm
			mapM_ (matchType tout) expst
			return stm'
		)
	matchType (TTuple tin) tintuple
	return $ EConstruction params stm'
checkType t (EEq a b) = do
	matchType t TBool
	(new, a') <- typeOf a
	b' <- checkType new b
	return $ EEq a' b'
checkType t (ENot e) = do
	matchType t TBool
	liftM ENot $ checkType TBool e
checkType t (EOr a b) = do
	matchType t TBool
	a' <- checkType TBool a
	b' <- checkType TBool b
	return $ EOr a' b'
checkType t (EAnd a b) = do
	matchType t TBool
	a' <- checkType TBool a
	b' <- checkType TBool b
	return $ EAnd a' b'
checkType t (ETernary c a b) = do
	c' <- checkType TBool c
	a' <- checkType t a
	b' <- checkType t b
	return (ETernary c' a' b')
checkType t e@EPoint = addTypeBound t e $ TOr [
		TFunction (TTuple []) TPoint,
		TFunction (TTuple [TLine]) TPoint,
		TFunction (TTuple [TCircle]) TPoint
	]
checkType t e@ECircle = addTypeBound t e $ TOr [
		TFunction (TTuple []) TCircle,
		TFunction (TTuple [TPoint]) TCircle,
		TFunction (TTuple [TPoint, TPoint]) TCircle
	]
checkType t e@ELine = addTypeBound t e $ TOr [
		TFunction (TTuple []) TLine,
		TFunction (TTuple [TPoint]) TLine,
		TFunction (TTuple [TPoint, TPoint]) TLine
	]
checkType t e@ERead = addTypeBound t e $ TOr [
		TFunction (TTuple []) TPoint,
		TFunction (TTuple []) TLine,
		TFunction (TTuple []) TCircle
	]
checkType t e@ECenter = addTypeBound t e $ TFunction (TTuple [TCircle]) TPoint
checkType t e@EWrite = addTypeBound t e $ TFunction (TUnknown) (TTuple [])
checkType t e@EIntersection = addTypeBound t e $ TOr [
		TFunction (TTuple [TLine, TLine]) (TOr [TTuple [], TPoint, TLine]),
		TFunction (TTuple [TLine, TCircle]) (TOr [TTuple [], TPoint, TTuple [TPoint, TPoint]]),
		TFunction (TTuple [TCircle, TLine]) (TOr [TTuple [], TPoint, TTuple [TPoint, TPoint]]),
		TFunction (TTuple [TCircle, TCircle]) (TOr [TTuple [], TPoint, TTuple [TPoint, TPoint], TCircle])
	]
checkType _ e@EIgnore = return e
checkType t e@ETrue = addTypeBound t e TBool
checkType t e@EFalse = addTypeBound t e TBool
checkType typ tree = error $ "checkType unimplemented for: " ++ show tree

addTypeBound :: Type -> Exp -> Type -> TypeEnv Exp
addTypeBound t e tt = do
	ttt <- matchType t tt
	return $ ETypeBound e ttt

--         returned type   exps before                 types of exps        types of exps before breaks/continues
retType :: Type ->         [Type] ->   Stm -> TypeEnv ([Type],         Stm, [Type])
retType ret t (SScope idents types stm) = do
	((exps, stm', breaks), _) <- newTypeScope idents types (retType ret t stm)
	return (exps, SScope idents types stm', breaks)
retType _ _ (SExp exp) = do
	(t, exp') <- ("in " ++ printTree exp) `try` typeOf exp
	return ([t], SExp exp', [])
retType ret _ (SReturn exp) = do
	exp' <- checkType ret exp
	return ([], SReturn exp', [])
retType _ t s@SEmpty = return (t, s, [])
retType ret t (SSequence stms) = do
	foldM (\(t, SSequence s, breaks) stm -> do
			(exps, stm', breaks') <- retType ret t stm
			return (exps, SSequence $ s ++ [stm'], breaks ++ breaks')
		) (t, SSequence [], []) stms
retType ret t (SIfElse c a b) = do
	c' <- checkType TBool c
	(expsa, a', breaksa) <- retType ret t a
	(expsb, b', breaksb) <- retType ret t b
	return (expsa ++ expsb, SIfElse c' a' b', breaksa ++ breaksb)
retType ret t cas@(SCase e cases) = do
	(new, e') <- ("in " ++ printTree e) `try` typeOf e
	(t', cases', types, breaks) <- liftM unzip4 $ mapM (\(CaseEntry e s) -> do
			(typ, e') <- typeOf e
			(t', s', breaks) <- retType ret t s
			return (t', CaseEntry (ETypeBound e' typ) s', typ, breaks)
		) cases
	matchType (TOr types) new
	return (concat t', SCase e' cases', concat breaks)
retType ret t (SWhile cond stm) = do
	cond' <- checkType TBool cond
	(t', stm', breaks) <- retType ret t stm
	return (t ++ t' ++ breaks, SWhile cond' stm', [])
retType ret t (SFor init cond inc stm) = do
	init' <- checkType TUnknown init
	cond' <- checkType TBool cond
	inc' <- checkType TUnknown inc
	(t', stm', breaks) <- retType ret t stm
	return (t ++ t' ++ breaks, SFor init' cond' inc' stm', [])
retType ret t SBreak = return (t, SBreak, t)
retType ret t SContinue = return (t, SContinue, t)
retType _ _ t = error $ "retType unimplemented for: " ++ show t

--maybeMatchType :: Maybe Type -> Maybe Type -> TypeEnv (Maybe Type)
--maybeMatchType Nothing Nothing = return $ Nothing
--maybeMatchType (Just a) Nothing = return $ Just a
--maybeMatchType Nothing (Just a) = return $ Just a
--maybeMatchType (Just a) (Just b) = liftM Just $ matchType a b

matchType a b = _matchType [] a b

--            dereferenced typedes, types to match
_matchType :: [Ident] ->            Type -> Type -> TypeEnv Type
_matchType visited _a _b = do
	a <- normalizeTypes _a
	b <- normalizeTypes _b
	case (a, b) of
		(TBool, TBool) -> return TBool
		(TPoint, TPoint) -> return TPoint
		(TLine, TLine) -> return TLine
		(TCircle, TCircle) -> return TCircle
		(TFunction tin tout, TFunction tin' tout') -> liftM2 TFunction (_matchType visited tin tin') (_matchType visited tout tout')
		(TTuple aa, TTuple bb) -> do
			when (length aa /= length bb) $ typeMatchError a b (Just "different lenght")
			t <- mapM (uncurry $ _matchType visited) (zip aa bb)
			return $ TTuple t

		(TOr aa, TOr bb) -> typeMatchErrorTry a b $ do
			let [shorter, longer] = sortWith length [aa, bb]
			--downcast <- pick $ inits shorter
			let downcast = shorter
			other <- pick $ permutations longer
			t <- sequence $ zipWith (_matchType visited) other downcast
			case t of
				[] -> guardList False >> return undefined
				[tt] -> return tt
				_ -> return $ TOr t

		(TTmp aa, TTmp bb) -> case compare aa bb of
			EQ -> return a
			LT -> insertBound bb a >> return a
			GT -> insertBound aa b >> return b

		(TIdent na, TIdent nb) -> case na == nb of
			True -> return a
			False -> do
				error $ show a ++ " " ++ show b
				aa <- derefTypedef na
				bb <- derefTypedef nb
				_matchType visited aa bb

		(TTmp ta, _) -> do
			insertBound ta b
			return b
		(_, TTmp _) -> _matchType visited b a

		(TIdent name, _) -> case elem name visited of
			True -> typeMatchError a b (Just "recursion")
			False -> do
				aa <- derefTypedef name
				_matchType (name:visited) {-visited-} aa b
		(_, TIdent _) -> _matchType visited b a

		(TOr ta, _) -> do
			t <- pick ta
			_matchType visited t b
		(_, TOr _) -> _matchType visited b a

		(_, _) -> typeMatchError a b Nothing

typeMatchError :: Type -> Type -> Maybe String -> TypeEnv a
typeMatchError a b s = do
	s' <- typeMatchErrorString a b
	throw $ s' ++ maybe "" (\t -> " (" ++ t ++ ")") s

typeMatchErrorString :: Type -> Type -> TypeEnv String
typeMatchErrorString a b = do
	a' <- normalizeTypes a
	b' <- normalizeTypes b
	return $ "couldnt match " ++ show a' ++ " with " ++ show b'

typeMatchErrorTry :: Type -> Type -> TypeEnv a -> TypeEnv a
typeMatchErrorTry a b env = do
	s <- typeMatchErrorString a b
	try s env

insertBound :: Integer -> Type -> TypeEnv ()
insertBound n t = S.modify (\(state, free) -> (M.insert n t state, free))

derefTypedef :: Ident -> TypeEnv Type
derefTypedef name = do
	t <- R.asks (M.lookup name . snd)
	case t of
		Just tt -> return tt
		Nothing -> error $ "derefType: not in scope: " ++ show name

--                tree to normalize
normalizeTypes :: Tree a ->         TypeEnv (Tree a)
normalizeTypes a = case a of
	TUnknown -> newType

	TTmp t -> do
		maybe_tt <- S.gets (M.lookup t . fst)
		case maybe_tt of
			Nothing -> return $ TTmp t
			Just (TTmp tt) -> if t <= tt then
				return (TTmp t) else
				normalizeTypes (TTmp tt)
			Just tt -> return tt

	TOr t -> do
		tt <- mapM normalizeTypes t
		return $ case nub tt of
			[] -> error "normalizeTypes: empty TOr"
			[ttt] -> ttt
			ttt -> TOr ttt

	_ -> composOpM normalizeTypes a

pick :: [a] -> TypeEnv a
pick t = lift $ lift $ lift $ (msum :: [L.ListT I.Identity a] -> L.ListT I.Identity a) (fmap pure t)

guardList = lift . guard
