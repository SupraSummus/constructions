#!/bin/bash

interpreter=${1:-./dist/build/constructions/constructions}

for file in `find test/ -name *.in`; do
	infile=$file
	outfile=${file%.in}.out
	progfile=${file%.*.in}.cc
	echo ${file%.in}
	diff <(cat $infile | $interpreter $progfile 2> /dev/null) $outfile
done

for file in `find test/ -name *.cc.err`; do
	echo $file
	out=`$interpreter $file 2> /dev/null`
	if [ $? -eq 0 -o -n "$out" ]; then
		echo "fail!"
	fi
done
