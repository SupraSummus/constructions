{-# LANGUAGE TypeFamilies #-}
module Exp (expVal, evalExp, eval, expLoc) where

import Constructions.Abs
import Control.Monad
import qualified Geometry as G
import Env
import {-# SOURCE #-} Stm
import Err
import qualified Data.Map as M
import qualified Control.Monad.Reader as R
import Control.Monad.IO.Class
import System.IO
import Control.Monad.Trans
import qualified Data.List as L

expVal :: Exp -> Env Val
expVal (EAssignment locexp valexp) = do
	loc <- expLoc locexp
	val <- expVal valexp
	loc val
	return val
expVal (ETernary cond a b) = do
	condv <- expVal cond
	case condv of
		VBool True -> expVal a
		VBool False -> expVal b
expVal (EEq a b) = VBool <$> liftM2 (==) (eval a) (eval b)
expVal (EOr a b) = binaryBool (||) a b
expVal (EAnd a b) = binaryBool (&&) a b
expVal (ENot e) = liftM (\(VBool v) -> VBool $ not v) (eval e)
expVal (ETuple exps) = liftM VTuple $ mapM evalExp exps
expVal (EConstruction idents stm) = do
	env <- R.ask
	return $ VFunction (_construction env) where
		_construction env args = R.local (const env) $ injectEnv (zip idents args) $ exec stm
expVal (ECall exp args) = do
	(VFunction f) <- eval exp
	vargs <- mapM eval args
	f vargs
expVal (EIdent (Ident name)) = ("when accessing variable " ++ show name) `try` do
	loc <- getLoc name
	getVal loc

-- builtins
expVal (ETypeBound ETrue TBool) = return $ VBool True
expVal (ETypeBound EFalse TBool) = return $ VBool False
expVal (ETypeBound ERead (TFunction (TTuple []) TPoint)) = return $ VFunction $ const $ do
		liftIO (hPutStr stderr "input point: ")
		[x, y] <- liftIO readLn
		return $ VPoint (G.Point x y)
expVal (ETypeBound EWrite (TFunction _ (TTuple []))) = return $ VFunction (\args -> do
		let str = L.intercalate ", " (fmap show args)
		liftIO (putStrLn str)
		return (VTuple [])
	)
expVal (ETypeBound EPoint (TFunction (TTuple []) TPoint)) = return $ VFunction (\[] -> return $ VPoint $ G.point)
expVal (ETypeBound EPoint (TFunction (TTuple [TLine]) TPoint)) = return $ VFunction (\[VLine l] -> return $ VPoint $ G.pointFromLine l)
expVal (ETypeBound EPoint (TFunction (TTuple [TCircle]) TPoint)) = return $ VFunction (\[VCircle c] -> return $ VPoint $ G.pointFromCircle c)
expVal (ETypeBound ELine (TFunction _ TLine)) = return $ VFunction (\args -> case args of
		[] -> return $ VLine $ G.line
		[VPoint a] -> return $ VLine $ G.lineThroughPoint a
		[VPoint a, VPoint b] -> if a == b then throw "line through equal points" else
			return $ VLine $ G.lineThroughPoints a b
	)
expVal (ETypeBound ECircle (TFunction or TCircle)) = return $ VFunction (\args -> case args of
		[] -> return $ VCircle $ G.circle
		[VPoint a] -> return $ VCircle $ G.circleWithCenter a
		[VPoint a, VPoint b] -> if a == b then throw "circle with r = 0" else
			return $ VCircle $ G.circleThroughPoints a b
	)
expVal (ETypeBound ECenter (TFunction (TTuple [TCircle]) TPoint)) = return $ VFunction (\[(VCircle c)] -> return (VPoint $ G.center c))
expVal (ETypeBound EIntersection (TFunction (TTuple [_, _]) tout)) = return $ VFunction (\args -> do
		let args' = case args of
			[VCircle b, VLine a] -> [VLine a, VCircle b]
			_ -> args
		let v = case args' of
			[VLine a, VLine b] -> case G.lineLineIntersection a b of
				G.LineLineIntersectionNull -> VTuple []
				G.LineLineIntersectionPoint p -> VPoint p
				G.LineLineIntersectionLine l -> VLine l
			[VCircle a, VCircle b] -> case G.circleCircleIntersection a b of
				G.CircleCircleIntersectionNull -> VTuple []
				G.CircleCircleIntersectionPoint p -> VPoint p
				G.CircleCircleIntersectionTwoPoints p pp -> VTuple [VPoint p, VPoint pp]
				G.CircleCircleIntersectionCircle c -> VCircle c
			[VLine a, VCircle b] -> case G.lineCircleIntersection a b of
				G.LineCircleIntersectionNull -> VTuple []
				G.LineCircleIntersectionPoint p -> VPoint p
				G.LineCircleIntersectionTwoPoints p pp -> VTuple [VPoint p, VPoint pp]
		let tout' = case tout of
			TOr t -> t
			t -> [t]
		case v of
			VTuple [] -> case elem (TTuple []) tout' of
				True -> return v
				False -> throw "intersection empty"
			VPoint _ -> case elem (TPoint) tout' of
				True -> return v
				False -> throw "intersection is single point"
			VTuple [VPoint _, VPoint _] -> case elem (TTuple [TPoint, TPoint]) tout' of
				True -> return v
				False -> throw "intersection is two points"
			VLine _ -> case elem TLine tout' of
				True -> return v
				False -> throw "intersection is line"
			VCircle _ -> case elem TCircle tout' of
				True -> return v
				False -> throw "intersection is circle"
	)

expVal exp = error $ "expVal not implemented for: " ++ show exp
evalExp = expVal
eval = expVal

binaryBool f a b = do
	(VBool va) <- evalExp a
	(VBool vb) <- evalExp b
	return $ VBool (f va vb)

expLoc :: Exp -> Env (Val -> Env ())
expLoc (EIdent (Ident name)) = return (\val -> do
		loc <- getLoc name
		setVal loc val
	)
expLoc (ETuple exps) = return (\(VTuple vals) -> do
		locs <- mapM expLoc exps
		sequence_ (zipWith ($) locs vals)
	)
expLoc EIgnore = return (const $ return ())
expLoc exp = error $ "expLoc not implemented for: " ++ show exp
