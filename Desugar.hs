{-# LANGUAGE TypeFamilies #-}
module Desugar (desugar) where

import Constructions.Abs

desugar :: Tree a -> Tree a
desugar a = case a of
	EmptyProgram -> StmProgram SEmpty
	
	-- ### stms ###
	
	SEmptyA _ -> SEmpty
	SEmptyB -> SEmpty
	SEmptyC -> SEmpty
	SEmptyD -> SEmpty
	S_Exp exp _ -> SExp (desugar exp)
	SSingleExp exp -> SExp (desugar exp)
	S_Sequence s1 s2 -> case desugar s2 of
		SSequence stms -> SSequence (desugar s1:stms)
		stm -> SSequence [desugar s1, stm]
	S_Control e _ -> desugar $ S_SingleControl e
	S_SingleControl (ReturnA e) -> SReturn (desugar e)
	S_SingleControl (ReturnB e) -> SReturn (desugar e)
	S_SingleControl Continue -> SContinue
	S_SingleControl Break -> SBreak
	
	S_Func (IdentTypeB ident typ) args stm -> desugar $ SExp $ EAssignment (EIdent ident) (EAnonymousATyped typ args stm)
	S_Func (IdentNoType ident) args stm -> desugar $ S_Func (IdentTypeB ident TUnknown) args stm
	
	S_IfA a b -> desugar $ SIfElse a b SEmpty
	S_IfB a b -> desugar $ SIfElse a b SEmpty
	S_IfElseB a b c d -> desugar $ S_IfElse a b c d
	S_IfElse a b NoElseIfs c  -> desugar $ SIfElse a b c
	S_IfElse a b (OneThenElseIf d e f g) c -> desugar $ S_IfElse a b (OneElseIf d e f g) c
	S_IfElse a b (OneElseIf _ d e f) c -> SIfElse (desugar a) (desugar b) (desugar $ S_IfElse d e f c)
	
	S_Case e (CaseBodyA b) -> desugar $ S_Case e (CaseBodyB b)
	S_Case e (CaseBodyB b) -> SCase (desugar e) (fmap desugar b)
	
	S_Typedef a b _ -> STypedef a (desugar b)
	
	-- ### exps ###
	
	E_Assignment e1 _ e2 -> EAssignment (desugar e1) (desugar e2)
	
	E_Or e1 _ e2 -> EOr (desugar e1) (desugar e2)
	E_And e1 _ e2 -> EAnd (desugar e1) (desugar e2)
	E_Eq e1 _ e2 -> EEq (desugar e1) (desugar e2)
	E_NEq e1 _ e2 -> ENot $ EEq (desugar e1) (desugar e2)
	
	E_Intersect a b -> ECall EIntersection [desugar a, desugar b]
	E_IntersectB a b -> ECall EIntersection [desugar a, desugar b]
	E_Circle e1 e2 -> ECall ECircle [desugar e1, desugar e2]
	E_CircleB e1 e2 -> ECall ECircle [desugar e2, desugar e1]
	E_Line a b -> ECall ELine [desugar a, desugar b]
	
	EUnary UnaryNot e -> ENot $ desugar e
	EUnary UnaryAnyCircle e -> ECall ECircle [desugar e]
	
	EAnonymousA idents stm -> desugar $ EAnonymousATyped TUnknown idents stm
	EAnonymousATyped typ idents stm -> ETypeBound
		(EConstruction (map stripType idents) (desugar stm))
		(TFunction (TTuple $ map (desugar . getType) idents) (desugar typ))
	EAnonymousB idents stm -> desugar $ EAnonymousA idents stm
	EAnonymousBTyped typ idents stm -> desugar $ EAnonymousATyped typ idents stm
	
	E_Call exp args -> ECall (desugar exp) (map desugar $ collectArgs args) where
	
	E_Tuple es -> ETuple $ map desugar (collectETupleBody es)
	E_EmptyTuple -> ETuple []
	E_SingleTuple e -> ETuple [desugar e]
	
	EAnyPoint -> ECall EPoint []
	EAnyLine -> ECall ELine []
	EAnyCircle -> ECall ECircle []
	
	E_SelectB a b -> desugar $ E_SelectC a b
	E_SelectC a FieldNamePoint -> ECall EPoint [desugar a]
	E_SelectC a FieldNameCenter -> ECall ECenter [desugar a]
	E_SelectC a (FieldNameIdent b) -> ESelect (desugar a) b
	
	-- ### types ###
	
	T_TupleSingle t -> TTuple [desugar t]
	T_TupleEmpty -> TTuple []
	T_Tuple body -> case collectTTupleBody body of
		(t, False) -> TTuple (desugar <$> t)
		(t, True) -> TInfiniteTuple (desugar <$> t)
	
	T_Function tin tout -> TFunction (TTuple (desugar <$> collectTFunctionArgs tin)) (desugar tout)
	
	T_Or body -> TOr (desugar <$> collectTOrBody body)
	
	_ -> composOp desugar a

stripType :: IdentWithMaybeType -> Ident
stripType (IdentNoType ident) = ident
stripType (IdentTypeB ident _) = ident

getType :: IdentWithMaybeType -> Type
getType (IdentNoType _) = TUnknown
getType (IdentTypeB _ t) = t

collectArgs :: CallArgs -> [Exp]
collectArgs NoArgs = []
collectArgs (OneArg e) = [e]
collectArgs (ManyArgs e es) = (e:collectArgs es)

collectETupleBody :: E_TupleBody -> [Exp]
collectETupleBody (E_TupleBodyTwo a b) = [a, b]
--collectETupleBody (E_TupleBodyTwoB a b) = [a, b]
collectETupleBody (E_TupleBodyMany e rest) = (e:collectETupleBody rest)

collectTTupleBody :: Tuple -> ([Type], Bool)
collectTTupleBody (TupleBodyInfinite _) = ([], True)
collectTTupleBody (TupleBodyBase a b) = ([a, b], False)
collectTTupleBody (TupleBodyCons a as) = ((a:bs), bt) where (bs, bt) = collectTTupleBody as

collectTFunctionArgs :: T_FunctionArgs -> [Type]
collectTFunctionArgs T_FunctionArgsEmpty = []
collectTFunctionArgs (T_FunctionArgsOne t) = [t]
collectTFunctionArgs (T_FunctionArgsMany t rest) = (t:collectTFunctionArgs rest)

collectTOrBody :: T_OrBody -> [Type]
collectTOrBody (T_OrBodyBase a b) = [a, b]
collectTOrBody (T_OrBodyCons a b) = (a:collectTOrBody b)
