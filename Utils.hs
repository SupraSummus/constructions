{-# LANGUAGE TypeFamilies #-}
module Utils where

import Constructions.Abs

unpackName :: Ident -> String
unpackName (Ident name) = name
