SANDBOX = $(shell pwd)/.cabal-sandbox
export PATH := /home/students/inf/PUBLIC/MRJP/ghc-7.10.2/bin:$(SANDBOX)/bin:$(PATH)

all:
	echo $(PATH)
	wget https://www.haskell.org/cabal/release/cabal-install-1.22.7.0/cabal-install-1.22.7.0.tar.gz -O cabal-install-1.22.7.0.tar.gz
	tar -xf cabal-install-1.22.7.0.tar.gz
	cd cabal-install-1.22.7.0/; ./bootstrap.sh --sandbox $(SANDBOX)
	which cabal
	cabal sandbox init
	cabal update
	cabal install
	cp $(SANDBOX)/bin/constructions ./interpreter
