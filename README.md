# Szkic języka

Ogólny pomysł jest taki, żeby stworzyć język konstrukcji klasycznych (za pomocą linijki bez podziałki i cyrkla). Kilka algorytmów takich konstrukcji było przedstawionych na początkowych wykładach z WPI i stąd inspiracja.

## Konstrukcje klasyczne

Konstrukcje klasyczne mają określony zbiór operacji elementarnych. Jest ich pięć:

 * mając dwa punkty konstruujemy prostą przez nie przechodzącą
 * mając dwa punkty `c` i `r` konstruujemy okrąg o środku `c` i promieniu `cr`
 * mając okrąg i prostą, dwa okręgi lub dwie proste możemy wyznaczyć ich punkty przecięcia (lub też stwierdzić że ich nie ma)

Z tych konstrukcji widać, że potrzebujemy co najmniej trzech typów: punkt, prosta, okrąg. Dodatkowo operacje wyznaczania punktów przecięcia mogą dać różne typy wyników. Np przecięcie dwóch okręgów to "nic" (krotka zeroelementowa, unit?), punkt, nieuporządkowana para punktów lub nieskończony zbiór (lista, krotka?) punktów. Z tego wynika, że potrzebujemy krotek (pominę nieuporządkowanie) oraz jakiejś formy typu będącego alternatywą typów.

## Sterowanie

Oprócz typów i konstrukcji geometrycznych potrzeba jakiegoś sterowania. Tutaj zapożyczę z języków imperatywnych `if`, `while` (`for`), `case` (z trochę inną semantyką). Razem z tymi konstrukcjami przydałby się także typ `boolean`, operator porównania, operatory logiczne.

## Struktury

Struktury to przydatna rzecz więc w moim języku też chcę je zawrzeć. Np `odcinek` to `struct {point a; point b;}` (składnia może będzie trochę inna).

## Kompletność w sensie Turinga

Mając `boolean`y, struktury i alternatywy typów można zbudować listy `boolean`ów. Te listy w połączeniu z operacjami logicznymi dają kompletność. Nie jest to dokładnie to co chciałem osiągnąć - zamysł był taki, żeby zmusić użytkownika do operowania konstrukcjami geometrycznymi i nie pozwolić mu wykonywać "standardowych" obliczeń.

## Lukier syntaktyczny?

Skoro już piszę gramatykę i mogę sobie pozwolić na wybryki to postanowiłem umożliwić użytkownikowi wyrazić program na wiele różnych sposobów. Jedną skrajnością jest w miarę zwięzła składnia w stylu C albo JavaScript'u (w niektórych przypadkach nawet zwięźlejsza), a drugą przegadany opis konstrukcji klasycznych, wyglądający niemal jak wyrażony za pomocą języka naturalnego (angielskiego). Mój język ma pozwolić na obie te skrajności oraz na ich mieszaniny.

# Konstrukcje

## `If`, `while`, `for`

Te konstrukcje zachowują się tak jak zazwyczaj. Konstrukcje `while` i `for` mogą mieć w środku `break` i `continue`. Konstrukcja `if` ma też wariant `if else` i `if elseif [...] else`.	

## `Case`

Mam mały problem z określeniem semantyki tej konstrukcji. Chcę, żeby służyła ona do rozpakowywania alternatywy typów czyli np:

    case of (a <bool or point>): // konstrukcja <> jest opisana niżej, w rozdziale o typach
    	b <bool>: ...; // a to bool
    	p <point>: ...; // a to point
    end

ale także ma służyć do rozpakowywania krotek:

    case of intersection(c1 <circle>, c2 <circle>):
    	(): ...; // okręgi rozłączne
    	p: ...; // okręgi styczne
    	p1, p2: ...; // okręgi mają dwa punkty wspólne
    	p1, p2, p3, ..: ...; // okręgi są identyczne (i maja nieskończenie wiele punktów wspólnych)	
    end

Analizując te dwa przykłady widać, że powstaje tu nieścisłość. Np w drugim przykładzie `p: ...;` może się dopasować do czegokolwiek poprzez wpisanie wynikowej krotki do `p`. Można dołożyć więzy na typ czyli napisać `p <point>: ...;` ale to nie jest tak wygodne.

Muszę pomyśleć jeszcze nad tą konstrukcją i ewentualnie ją zmienić (np rozbić na dwie różne konstrukcje).

## Deklaracje funkcji

W języku "funkcję" nazywam "konstrukcją". (Ewentualnie mogę dodać `FuncKeyword ::= "construction" | "function";`.) Funkcje mogą być nazwane i anonimowe. Oczywiście funkcje anonimowe można przypisać do zmiennych tworząc funkcje nazwane.

## `Typedef` i typy

`typedef` to, klasycznie, alias typu.

Wbudowane typy to `point`, `line`, `circle`, `boolean`, krotka (także pusta, jednoelementowa oraz nieskończona), alternatywa (taki wieloelementowy `Either`), funkcja.

Struktura to typ definiowany przez użytkownika. Struktury są zawsze "anonimowe". Żeby je nazwać trzeba użyć `typedef`a.

W wyrażeniach można dodawać więzy na typy. W zależności od miejsca występowania ma to służyć odpakowaniu typu z alternatywy albo po prostu dopisania "komentarza" ułatwiającego rozumienie kodu. Zapewne zdarzą się też przypadku w których rekonstrukcja typów nie da sobie rady - wtedy użytkownik będzie musiał dołożyć te więzy na typ. Ogólna (uproszczona) postać takiego wiązania to `ETypeBound.Exp ::= Exp "<" Type ">";`.

## Wyrażenia

 * Przypisanie. Przypisania mogą służyć do rozpakowywania krotek (jak w Pythonie) i rozpakowywania z alternatywy. W razie nieudanego rozpakowania będą zgłaszać błędy wykonania.
 * Konstruktor krotki: `(a, b, c,)`, `(a,)`, `()`.
 * Operator "ternary": `condition ? when_true : when_false`.
 * Porównanie i nierówność.
 * Logiczny `and` i `or`.
 * Cała gama binarnych, przegadanych operatorów do konstrukcji: `a intersected with b`, `line through a and b`, ...
 * Operatory unarne: `not`, cała gama przegadanych operatorów w stylu `any point from` albo `any line through`.
 * Więzy na typ (o których jest w rozdziale dot. typów)
 * Operator wybrania pola ze struktury. Kropka.
 * Wywołanie funkcji.
 * Definicja funkcji anonimowej.
 * Konstruktor instancji struktury. `{polea: wartosca; poleb: wartoscb}`
 * Identyfikator
 * Wbudowane stałe: `true`, `false`, `intersection`, `circle`, ...

# Realizacja

## Operacje geometryczne i związane typy danych

Zbiór tzw liczb konstruowalnych oraz zbudowane na jego podstawie zbiory punktów (para), linii (trójka) i okręgów (punkt + promień) są zamknięte ze względu na operacje dozwolone przez konstrukcje klasyczne. Liczby konstruowalne są postaci `a + b * sqrt(c)` gdzie `a`, `b` i `c` są liczbami wymiernymi. Można je dokładnie przechowywać w pamięci. W mojej implementacji wybrałem właśnie taki sposób reprezentacji wartości. Dodatkowym ułatwieniem jest fakt, że powstała już biblioteka dla haskella obsługująca liczby konstruowalne.

Operacje elementarne na obiektach geometrycznych to po prostu rozwiązywanie (dość trywialnych) układów równań.

## Kontrola typów

Kontrola typów jest statyczna. Typów nie trzeba wszędzie podawać - są rekonstruowane. Zmienne deklarowane są automatycznie przy pierwszym ich użyciu (także przy odczycie).

Dużo problemów sprawił mi typ "alternatywa typów". Do dedukcji do jakiego typu faktycznie sprowadzi się ten typ wykorzystałem monadę list. W przypadku wątpliwości obliczenie jest "forkowane". Nie pracowałem dużo nad sposobem komunikacji błędów dedukcji typów co w połączeniu z wieloma gałęziami wykonania algorytmu daje dość nieczytelne komunikaty. Czasem też pojawia się błąd gdy wiele gałęzi zakończyło się sukcesem (co prawdopodobnie oznacza, że program nie jest jednoznaczny).

Innym źródłem problemów były funkcje rodzaju `intersection(a, b)`, które mogą przyjmować argumenty różnych typów, albo nawet różną liczbę argumentów.

`typedef` i typy reukurencyjne nie są dobrze przetestowane. Napisałem tylko jeden test (implementujący listy) i miałem problem, żeby się dobrze otypował.

Zwracanie wartości z funkcji może odbywać się na dwa sposoby: przez `return` oraz przez zakończenie funkcji - w drugim przypadku zwracana jest ostatnio wyliczona wartość (upraszczając trochę). Te dwa sposoby zwracania wyniku też podlegają kontroli typów jednak lepiej używać "returna" bo w niektórych przypadkach drugi sposób źle się typuje.

## Zmiany względem planu

 * Zrezygnowałem z nieskończonych krotek. Przecięcie linii i linii to nic, punkt lub linia. Dla okręgów podobnie.

## Czego nie ma, co można poprawić

 * Odśmiecarka. Teraz lokacje są zajmowane i zwalniane dopiero na przy zakończeniu programu.
 * Lepsze komunikaty o błędach. (+ miejsce wystąpienia w kodzie)
 * Komunikaty błędu wykonania z backtracem i miejscem w treści programu
 * Naprawiony bug sprawdzania typów dla wartości zwracanych jako "ostatnia wyliczona wartość"
 * Rekordy.
 * Przetestowane typy rekurencyjne.
 * Konstrukcja pozwalająca na wypisywanie stringów na standardowe wyjście. (Trudno się debuguje programy bez tego.)

## Kompilacja i uruchomienie

Interpreter jest w postaci paczki cabala. Na współczesnej wersji cabala wystarczy uruchomić `cabal sandbox init; cabal install`, jednak w labach nie ma współczesnej wersji. W `Makefile` jest prosty skrypcik, który ściąga i instaluje lokalnie cabala, po czym uruchamia kompilację projektu - zatem `make` powinien działać w labach.

## Testy/przykłady

Przykładowe programy można znaleźć w katalogu `test/`. Pliki `.cc` zawierają treść programu, a pliki `.in` i `.out` odpowiednio wejście i wyjście do automatycznego testowania. W katalogu `test/static_fail/` znajdują się przykłady programów, które są niepoprawne jeszcze przed rozpoczęciem wykonania. Najbardziej zaawansowany program przykładowy to chyba `test/segment_math/gcd.cc` implementujący algorytm Euklidesa w którym liczby reprezentowane są przez długość odcinków. Można automatycznie uruchomić wszystkie testy za pomocą skryptu `./test.sh [komenda interpretera]` (czyli w sumie `./test.sh ./interpreter`).

## Repozytorium git

Można je znaleźć pod adresem <https://bitbucket.org/SupraSummus/constructions/>.
