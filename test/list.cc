typedef list () or (list,);

construction zero <list> ()
	return ();

construction inc <list> (a <list>) {
	return (a,);
}

construction sum <list> (a <list>, b <list>) {
	case of a {
		(): return b;
		(tail,): return (sum(tail, b),);
	}
}

construction hasNext (a)
	case of a:
		_ <()>: false;
		_: true;
	end

construction forprint (a) {
	for (i := a; hasNext(i); (i,) := i)
		write(true);
	return ();
}

write(zero()).

five := sum(
	inc(inc(zero())),
	inc(inc(inc(zero())))
).

write(five).

forprint(five).
