c := read();
start := read();

prev := start.
curr, _ := intersection(circle(c,start), circle(start,c)).
while curr doesn't equal start do
	write(prev).
	t1, t2 is intersection(circle(c, curr), circle(curr, c)).
	if t1 equals prev then next is t2. else next is t1.
	prev is curr.
	curr is next.
end.
write(prev).
