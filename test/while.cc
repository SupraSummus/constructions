a := true.
while (true) {
	if (false) break;
	write(true);
	if (true) break;
	a := false.
	break;
}
write(a).

while (false) {write(false)};

b := false;
while (true) {
	if (not b) {
		b := true;
		continue;
	}
	write(b);
	break;
	write(false);
}
