make_flipper := construction () {
	i := false;
	return (
		construction () {return i},
		construction () {i := not i}
	);
};

(getA, flipA) := make_flipper();
(getB, flipB) := make_flipper();

write((getA(), getB())).
flipA().
write((getA(), getB())).
flipB().
write((getA(), getB())).
flipB().
write((getA(), getB())).
