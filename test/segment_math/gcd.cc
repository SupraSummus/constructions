// ### function declarations ;D? ###

typedef segment (point, point).
if (false) { // prevent from reading unitialised values... it's funny
	main            <->()>.
	gcd             <segment, segment -> segment>.
	is_zero         <segment -> bool>.
	mod             <segment, segment -> segment>.
	gt              <segment, segment -> bool>.
	unify           <segment, segment -> point, point, point>.
	closer          <point, point, point -> point>.
	point_in_circle <circle, point -> bool>.
	bisector        <point, point -> line>.
	sub             <segment, segment -> segment>.
	translate       <point, segment -> point>.
	other_point     <(point, point), point -> point>.
	vector_add      <point, point, point -> point>.
}

// ### prog ###

main := construction () {
	zero := read() <point>.
	a := read() <point>.
	b := read() <point>.
	(p, k) := gcd((zero, a), (zero, b)).
	res := translate(k, (p, zero)).
	write(res).
}.

// ### implementation ###

gcd is construction (a, b):
	while not is_zero(b):
		c is b.
		b is mod(a, b).
		a is c.
	end
	result is a.
end.

// a == 0
is_zero is construction (a):
	p, k is a.
	p = k.
end.

// a mod b
mod is construction (a, b):
	while gt(a, b) : // not a very fast mod
		a := sub(a, b).
	end
	result is a.
end.

// a > b
gt is construction (a, b):
	zero, ka, kb is unify(a, b).
	closer(zero, ka, kb) = kb.
end.

// given two segments projects them to one line preserving their lengths
// returns a tuple of points: (common end, end of a, end of b)
unify is construction (a, b):
	zero, ka is a.
	tpb, tkb is b.
	if is_zero(a) then return tpb, tpb, tkb.
	if is_zero(b) then return zero, ka, zero.

	kb is translate(tkb, (tpb, zero)).
	kb1, kb2 is intersection of
		line through zero and ka and
		circle with center zero through kb.
	kb_final is closer(ka, kb1, kb2).
	result is zero, ka, kb_final.
end.

// returns point closer to zero
construction closer <point> (zero <point>, a <point>, b <point>) {
	if zero = a then zero; else
	point_in_circle(circle(zero, a), b) ? b : a;
}.

// is point in circle
construction point_in_circle <bool> (c <circle>, p <point>) {
	if (c center = p) return true;

	radial := line(p, center(c));
	p0, p2 := intersection(radial, circle(p, c center));
	prep := bisector(p0, p2);
	case of intersection(c, prep) {
		_, _: true;
		_ <point>: true;
		(): false;
	};
}.

bisector is construction (a, b) {
	c, d is intersection(circle(a,b), circle(b,a));
	return line(c,d).
}.

// a - b
// if a < b the result will be b - a
// it's realy abs(a - b)
sub is construction (a, b) {
	zero, ka, kb is unify(a, b).
	return (ka, kb).
}.

// translates point by a vector
translate is construction (p, v):
	pv, kv is v.
	result is vector_add(pv, p, kv).
end.

vector_add is construction given common, a, b:
	if a equals b do
		if a equals common then result is common.
		else result is other_point(
			line through common and a intersected with circle through common with center a,
			common).
	end else do
		middle is bisector(a, b) intersected with line through a and b.
		if middle equals common then result is middle.
		else result is other_point(
			intersection of line through middle and common and circle with center middle through common,
			common).
	end
end.

other_point := construction (pp, p) {
	a, b := pp;
	if a = p then b. else a.
}.

// ### exec ###

main().
