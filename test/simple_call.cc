construction a <bool> () {
	false;
	true;
}

write(a());

construction b <bool> () {
	return true;
	false;
}

write(b());

write(construction () {
	true;
	if false then false;
}());

write(construction () {
	false;
	if false then false; else true;
}());

write(construction () {
	if false then return false;
	true;
}());
