//                                        point, point
bisector is construction producing line given a, b:
	c, d is
		circle with center a through b intersected with
		circle with center b through a.
	result is line through c and d.
end.

// is point in circle
construction point_in_circle <bool> (c <circle>, p <point>) {
	if (c center = p) return true;

	radial := line(p, center(c));
	p0, p2 := intersection(radial, circle(p, c center));
	prep := bisector(p0, p2);
	case of intersection(c, prep) {
		_, _: true;
		_ <point>: true;
		(): false;
		(): false;
		(): false;
	};
}

// returns point closer to zero
construction closer <point> (zero <point>, a <point>, b <point>) {
	if zero = a then zero; else
	point_in_circle(circle(zero, a), b) ? b : a;
}

c := read();
a := read();
b := read();
write(closer(c, a, b));
