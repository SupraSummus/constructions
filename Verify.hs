{-# LANGUAGE TypeFamilies #-}
module Verify (verify) where

import Constructions.Abs

verify :: Tree a -> IO ()
verify a = sequence_ $ zipWith ($) [
	lValues,
	returns,
	nestedTypedefs,
	breakContinue,
	const (return ())] (repeat a)

lValues :: Tree a -> IO ()
lValues a = case a of
	EAssignment e _ -> isLValue e
	CaseEntry e _ -> isLValue e
	_ -> composOpM_ lValues a

isLValue :: Tree a -> IO ()
isLValue a = case a of
	EIdent _ -> return ()
	ETuple e -> mapM_ isLValue e
	ETypeBound e _ -> isLValue e
	EIgnore -> return ()
	_ -> fail (show a ++ " is not lvalue")

returns :: Tree a -> IO ()
returns a = case a of
	SReturn _ -> fail "return outside function"
	EConstruction _ _ -> return ()
	_ -> composOpM_ returns a

nestedTypedefs :: Tree a -> IO ()
nestedTypedefs a = case a of
	EConstruction _ s -> noTypedefsIn s
	_ -> composOpM_ nestedTypedefs a

noTypedefsIn :: Tree a -> IO ()
noTypedefsIn a = case a of
	SScope _ t s -> case t of
		[] -> noTypedefsIn s
		_ -> fail $ "nested typedef: " ++ show a
	_ -> composOpM_ noTypedefsIn a

breakContinue :: Tree a -> IO ()
breakContinue a = case a of
	SBreak -> fail "bare break"
	SContinue -> fail "bare continue"
	SWhile _ _ -> return ()
	SFor _ _ _ _ -> return ()
	_ -> composOpM_ breakContinue a
