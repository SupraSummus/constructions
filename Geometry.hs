module Geometry where

import Data.Real.Constructible

data Point = Point {x :: Construct, y :: Construct} deriving (Eq, Show)

distance :: Point -> Point -> Construct
distance (Point x1 y1) (Point x2 y2) = sqrt ((x1 - x2)^2 + (y1 - y2)^2)

point :: Point
point = Point 0 0

pointFromLine :: Line -> Point
pointFromLine line@(Line a b c)
	| b /= 0 = Point 0 ((-c) / b)
	| a /= 0 = Point ((-c) / a) 0
	| otherwise = error $ "invalid line: " ++ show line

pointFromCircle :: Circle -> Point
pointFromCircle (Circle (Point x y) r) = Point (x + r) y

-- xa + yb + c = 0
data Line = Line {a :: Construct, b :: Construct, c :: Construct} deriving (Show)

instance Eq Line where
	l1 == l2 = case lineLineIntersection l1 l2 of
		LineLineIntersectionLine _ -> True
		_ -> False

line :: Line
line = lineThroughPoint point

lineThroughPoint :: Point -> Line
lineThroughPoint (Point x y) = Line 1 0 (-x) -- vertical 

lineThroughPoints :: Point -> Point -> Line
lineThroughPoints (Point xa ya) (Point xb yb)
	-- (y - ya)(xb - xa) - (yb - ya)(x - xa) = 0
	-- y(xb - xa) -ya(xb - xa) -x(yb - ya) + xa(yb - ya) = 0
	-- x(-yb + ya) + y(xb - xa) -ya(xb - xa)  + xa(yb - ya) = 0
	--                          -ya xb + ya xa + xa yb - xa ya
	--                          -ya xb + yb xa
	| a == 0 && b == 0 = error "lineThroughPoints: same points"
	| otherwise = Line a b c where
		a = ya - yb
		b = xb - xa
		c = yb * xa - ya * xb

data Circle = Circle {center :: Point, radius :: Construct} deriving (Show)

instance Eq Circle where
	Circle c r == Circle c' r' = c == c' && (r == r' || r == (-r'))
	_ == _ = False

circle :: Circle
circle = circleWithCenter point

circleWithCenter :: Point -> Circle
circleWithCenter a = Circle a 1

circleThroughPoints :: Point -> Point -> Circle
circleThroughPoints c pr
	| c == pr = error "circleThroughPoints: raduis 0"
	| otherwise = Circle c (distance c pr)

data LineLineIntersection
	= LineLineIntersectionNull
	| LineLineIntersectionPoint Point
	| LineLineIntersectionLine Line
	deriving (Eq, Show)

data LineCircleIntersection
	= LineCircleIntersectionNull
	| LineCircleIntersectionPoint Point
	| LineCircleIntersectionTwoPoints Point Point
	deriving (Show)

instance Eq LineCircleIntersection where
	LineCircleIntersectionNull == LineCircleIntersectionNull = True
	LineCircleIntersectionPoint p1 == LineCircleIntersectionPoint p2 = p1 == p2
	LineCircleIntersectionTwoPoints a1 b1 == LineCircleIntersectionTwoPoints a2 b2 = a1 == a2 && b1 == b2 || a1 == b2 && b1 == a2
	_ == _ = False

data CircleCircleIntersection
	= CircleCircleIntersectionNull
	| CircleCircleIntersectionPoint Point
	| CircleCircleIntersectionTwoPoints Point Point
	| CircleCircleIntersectionCircle Circle
	deriving (Show)

instance Eq CircleCircleIntersection where
	CircleCircleIntersectionNull == CircleCircleIntersectionNull = True
	CircleCircleIntersectionPoint p1 == CircleCircleIntersectionPoint p2 = p1 == p2
	CircleCircleIntersectionTwoPoints a1 b1 == CircleCircleIntersectionTwoPoints a2 b2 = a1 == a2 && b1 == b2 || a1 == b2 && b1 == a2
	CircleCircleIntersectionCircle c1 == CircleCircleIntersectionCircle c2 = c1 == c2
	_ == _ = False

lineLineIntersection :: Line -> Line -> LineLineIntersection
lineLineIntersection (Line a b c) (Line a' b' c')
	| wab /= 0 = LineLineIntersectionPoint $ Point (wbc / wab) (wca / wab)
	| wbc /= 0 = LineLineIntersectionNull
	| otherwise = LineLineIntersectionLine (Line a b c) where
		wab = a * b' - a' * b
		wbc = b * c' - b' * c
		wca = c * a' - c' * a

lineCircleIntersection :: Line -> Circle -> LineCircleIntersection
lineCircleIntersection (Line a b c) (Circle (Point cx cy) r)
	-- http://www.mathematik.tu-darmstadt.de/~ehartmann/cdgen0104.pdf page 17
	| d2 < 0 = LineCircleIntersectionNull
	| d2 == 0 = LineCircleIntersectionPoint $ Point (cx + a * c' / z) (cy + b * c' / z)
	| otherwise = let t = (sqrt d2) / z in LineCircleIntersectionTwoPoints
		(Point (cx + a * c' / z + b * t) (cy + b * c' / z - a * t))
		(Point (cx + a * c' / z - b * t) (cy + b * c' / z + a * t))
	where
		c' = (-c) - a * cx - b * cy -- -4 - -8 = 4
		d2 = r^2 * (a^2 + b^2) - c'^2 -- 16 - 16 = 0
		z = (a^2 + b^2)

circleCircleIntersection :: Circle -> Circle -> CircleCircleIntersection
circleCircleIntersection (Circle (Point x1 y1) r1) (Circle (Point x2 y2) r2)
	-- http://www.mathematik.tu-darmstadt.de/~ehartmann/cdgen0104.pdf page 18
	| a == 0 && b == 0 && c == 0 = CircleCircleIntersectionCircle (Circle (Point x1 y1) r1)
	| a == 0 && b == 0 = CircleCircleIntersectionNull
	| otherwise = case lineCircleIntersection (Line a b (-c)) (Circle (Point x1 y1) r1) of
		LineCircleIntersectionNull -> CircleCircleIntersectionNull
		LineCircleIntersectionPoint p -> CircleCircleIntersectionPoint p
		LineCircleIntersectionTwoPoints p1 p2 -> CircleCircleIntersectionTwoPoints p1 p2
	where
		a = 2 * (x2 - x1)
		b = 2 * (y2 - y1)
		c = r1^2 {- błąd w pdfie -} - x1^2 - y1^2 - r2^2 + x2^2 + y2^2
