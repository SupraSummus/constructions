{-# LANGUAGE TypeFamilies #-}
module DoScopes (doScopes) where

import Constructions.Abs
import qualified Control.Monad.State as ST
import qualified Data.Set as S
import Control.Monad
import qualified Data.Map as M

type ScopeEnv = ST.State (
		S.Set Ident, -- vars defined in scopes
		S.Set Ident, -- vars to be defined
		M.Map Ident Type -- typedefs
	)

doScopes :: Tree a -> Tree a
doScopes t = ST.evalState (_doScopes t) (S.empty, S.empty, M.empty)

_doScopes :: Tree a -> ScopeEnv (Tree a)
_doScopes a = case a of
	StmProgram s -> liftM StmProgram $ newScope [] s
	EConstruction idents s -> liftM (EConstruction idents) $ newScope idents s
	EIdent ident -> do
		has <- ST.gets (\(a, _, _) -> S.member ident a)
		when (not has) $ ST.modify (\(a, b, c) -> (a, S.insert ident b, c))
		return a
	STypedef ident typ -> do
		ST.modify (\(a, b, c) -> (a, b, M.insert ident typ c))
		return SEmpty
	_ -> composOpM _doScopes a

newScope :: [Ident] -> Stm -> ScopeEnv Stm
newScope idents a = do
	st <- ST.get
	ST.modify (\(a, b, c) -> (
			S.union b $ S.union (S.fromList idents) a, S.empty,
			M.empty
		))
	t <- _doScopes a
	(_, new, newt) <- ST.get
	ST.put st
	return $ SScope (S.toList new) (uncurry ScopeTypedef <$> M.toList newt) t
