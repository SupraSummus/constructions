{-# LANGUAGE TypeFamilies #-}
module Env (
	Loc, Env, Val(..),
	runEnv,
	setVal, getVal,
	getLoc, getLocRaw, newScope, injectEnv
) where

import qualified Control.Monad.Reader as R
import qualified Control.Monad.State.Lazy as S
import Err
import qualified Data.Map as M
import qualified Data.Functor.Identity as I
import Constructions.Abs
import Control.Monad
import Control.Monad.Trans
import Geometry as G

type Except = String
type Loc = Int
type LocMap = M.Map String Loc
data Mem = Mem {mem :: M.Map Loc Val, free :: Loc} deriving Show
type Env a =
	(ErrT
	(R.ReaderT LocMap
	(S.StateT Mem
	(IO)
	)
	)
	) a

data Val
	= VBool Bool
	| VTuple [Val]
	| VPoint G.Point
	| VLine G.Line
	| VCircle G.Circle
	| VFunction ([Val] -> Env Val)

instance Show Val where
	show (VBool b) = show b
	show (VTuple t) = show t
	show (VPoint p) = show p
	show (VLine l) = show l
	show (VCircle c) = show c
	show (VFunction _) = "function ..."

instance Eq Val where
	VBool a == VBool b = a == b
	VTuple a == VTuple b = a == b
	VPoint a == VPoint b = a == b
	VLine a == VLine b = a == b
	VCircle a == VCircle b = a == b
	VFunction _ == VFunction _ = error "compare VFunction"
	_ == _ = False

runEnv :: Env a -> IO (a, Mem)
runEnv execution = do
	let noExcept = runErrT execution
	let noReader = R.runReaderT noExcept (M.empty)
	let noState = S.runStateT noReader (Mem {mem = M.empty, free = 0})
	noIO <- noState
	case noIO of
		(Right v, state) -> return (v, state)
		(Left e, state) -> fail $ "runtime error: " ++ show e

setVal :: Loc -> Val -> Env ()
setVal loc val = S.modify (\state@(Mem mem _ ) -> state {mem = M.insert loc val mem})

getVal :: Loc -> Env Val
getVal loc = do
	val <- S.gets $ M.lookup loc . mem
	case val of
		Nothing -> throw $ "invalid read from location " ++ show loc
		Just x -> return x

alloc :: Env Loc
alloc = do
	freeLocation <- S.gets free
	S.modify (\state -> state {free = freeLocation + 1})
	return freeLocation

injectEnv :: [(Ident, Val)] -> Env a -> Env a
injectEnv [] block = block
injectEnv ((Ident name, val):defs) block = do
	loc <- alloc
	setVal loc val
	injectEnv defs $ R.local (M.insert name loc) block

getLocRaw :: String -> Env (Maybe Loc)
getLocRaw name = R.asks $ M.lookup name

getLoc :: String -> Env Loc
getLoc name = do
	loc <- R.asks $ M.lookup name
	case loc of
		Nothing -> throw $ "not in scope: " ++ name
		Just x -> return x

setLoc :: String -> Loc -> Env a -> Env a
setLoc name loc computation = R.local (M.insert name loc) computation

newScope :: [Ident] -> Env a -> Env a
newScope [] block = block
newScope ((Ident name):names) block = do
	loc <- alloc
	newScope names $ R.local (M.insert name loc) block
