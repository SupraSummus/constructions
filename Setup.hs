import Distribution.Simple
import Distribution.Simple.Setup
import Distribution.PackageDescription
import System.Process (system)

main :: IO ()
main = defaultMainWithHooks $ simpleUserHooks {
  preBuild = \_ flags -> do
      system $ "bnfc --haskell-gadt -o " ++ fromFlag (buildDistPref flags) ++ "/build/autogen -d Constructions.cf"
      return emptyHookedBuildInfo
}
