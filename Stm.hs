{-# LANGUAGE TypeFamilies #-}
module Stm (exec, execProg) where

import Constructions.Abs
import Exp
import Env
import Control.Monad
import Utils
import Control.Monad.Trans
import Err

_exec :: (Val -> Env Val) -> -- what to do in case of break stm
         (Val -> Env Val) -> -- what to do in case of continue stm
         Val -> -- previous result
         Stm -> -- this stm
         (Val -> Env Val) -> -- what to do next
         Env Val
_exec break continue prev a next = let
		justExec = _exec break continue
		execStm stm = _exec break continue prev stm next
	in case a of
		SScope names _ stm -> newScope names $ execStm stm
		SSequence [] -> next prev
		SSequence (stm:stms) -> justExec prev stm (\v -> justExec v (SSequence stms) next)
		SEmpty -> next prev
		SExp exp -> do
			v <- eval exp
			next v
		SIfElse c t f -> do
			v <- eval c
			case v of
				VBool True -> execStm t
				VBool False -> execStm f
		SReturn exp -> eval exp

		SCase exp options -> do
			v <- eval exp
			let typ = typeOf v
			matchCase v typ options where
				matchCase :: Val -> Type -> [CaseEntry] -> Env Val
				matchCase _ _ [] = throw "no options in case"
				matchCase v t ((CaseEntry (ETypeBound e tt) stm):rest) = do
					case typesMatch t tt of
						True -> do
							loc <- expLoc e
							loc v
							execStm stm
						False -> matchCase v t rest

		while@(SWhile cond stm) -> do
			(VBool v) <- eval cond
			let nextLoop = \v -> justExec v while next
			case v of
				False -> next prev
				True -> _exec next nextLoop prev stm nextLoop

		SFor init cond inc stm -> do
				eval init
				loop prev
			where
				loop v = do
					(VBool c) <- eval cond
					case c of
						False -> next v
						True -> _exec next incAndLoop v stm incAndLoop
				incAndLoop v = do
					eval inc
					loop v

		SBreak -> break prev
		SContinue -> continue prev

		_ -> error $ "execStm not implemented for: " ++ show a

exec :: Stm -> Env Val
exec stm = _exec (error "no loop to break") (error "no loop to continue") (VTuple []) stm return

execProg :: Program -> Env Val
execProg (StmProgram stm) = exec stm

typeOf :: Val -> Type
typeOf (VBool _) = TBool
typeOf (VPoint _) = TPoint
typeOf (VLine _) = TLine
typeOf (VCircle _) = TCircle
typeOf (VTuple v) = TTuple $ fmap typeOf v
typeOf (VFunction _) = error "typeOf function"

typesMatch :: Type -> Type -> Bool
typesMatch TBool TBool = True
typesMatch TPoint TPoint = True
typesMatch TLine TLine = True
typesMatch TCircle TCircle = True
typesMatch (TTuple a) (TTuple b) = length a == length b && (all id $ zipWith typesMatch a b)
typesMatch (TTmp _) _ = True
typesMatch _ (TTmp _) = True
typesMatch (TIdent _) _ = True
typesMatch _ (TIdent _) = True
typesMatch (TOr t) tt = any (typesMatch tt) t
typesMatch tt t@(TOr _) = typesMatch t tt
typesMatch _ _ = False
